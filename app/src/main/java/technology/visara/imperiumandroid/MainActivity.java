package technology.visara.imperiumandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{
    GoogleApiClient mGoogleApiClient;


    private EditText edMail, edPassword;
    private Button buttonConnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);// Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("64132778888-0ltipkon5somh2vifp3rfu7vseli3g86.apps.googleusercontent.com")
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //TODO: Voir pour les fragment activity.
        OptionalPendingResult<GoogleSignInResult> pendingResult =
                Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (pendingResult.isDone()) {
            envoieInfoWebService(pendingResult.get());
        } else {
            // There's no immediate result ready, displays some progress indicator and waits for the
            // async callback.
            pendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult result) {
                    envoieInfoWebService(result);

                }
            });
        }
        // Set the dimensions of the sign-in button.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                }
            }
        });

        edMail = (EditText)findViewById(R.id.editTextEmail);
        edPassword = (EditText)findViewById(R.id.editTextPassWord);
        buttonConnexion = (Button) findViewById(R.id.buttonConnexion);

        buttonConnexion.setOnClickListener(this);


    }

    private void envoieInfoWebService(GoogleSignInResult result){
        /*if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String mIdtest = acct.getIdToken();
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url + "utilisateur?google_token=" + mIdtest, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equalsIgnoreCase("true")) {
                        Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(i);
                    }else{
                        Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice : " + response, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Antony", "ça change" + error.getMessage());
                    Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice", Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(stringRequest);
        }*/
        Intent i = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(i);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 1); //RC_SIGN_IN
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from
        //   GoogleSignInApi.getSignInIntent(...);

        if (requestCode == 1) {
            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
            startActivity(i);
            /*GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String mIdtest = acct.getIdToken();
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                StringRequest stringRequest = new StringRequest(Request.Method.POST,  url+"utilisateur?google_token="+mIdtest, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equalsIgnoreCase("true")){
                            Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice : " + response, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Antony", "ça change = " + error.getMessage());
                        Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice", Toast.LENGTH_SHORT).show();
                    }
                });
                requestQueue.add(stringRequest);
            }else{
                Toast.makeText(MainActivity.this, "Resultat de la connexion erreur", Toast.LENGTH_SHORT).show();
            }*/
        }else{
            Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice : ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice : ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        String email = edMail.getText().toString();
        String motdepasse = edPassword.getText().toString();

        /*StringRequest stringRequest = new StringRequest(Request.Method.POST,  url+"connexion?email="+email+"&motdepasse=" +motdepasse, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(!response.equalsIgnoreCase("false")){
                    Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                    i.putExtra("personne", response);
                    startActivity(i);
                }else{
                    Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice : " + response, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Antony", "ça change = " + error.getMessage());
                Toast.makeText(MainActivity.this, "Erreur lors de la connexion au webservice", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/

    }
}
